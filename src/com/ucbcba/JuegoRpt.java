package com.ucbcba;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JuegoRpt {

    public static final ArrayList<String> VALID_PLAYS =
            new ArrayList<>(Arrays.asList("P","R","T"));
    public static final ArrayList<String> PLAYER_TWO_WINS =
            new ArrayList<>(Arrays.asList("PR","RT","TP"));

    public List jugar(List<List> jugada) throws WrongPlay, WrongNumberOfPlayers {

        String jugada21 = (String)jugada.get(1).get(1)
                + (String)jugada.get(0).get(1);;
        if(!VALID_PLAYS.contains(jugada.get(0).get(1)) ||
                !VALID_PLAYS.contains(jugada.get(1).get(1))
                ){
            throw new WrongPlay();
        }
        if(jugada.size()!= 2){
            throw new WrongNumberOfPlayers();
        }
        if (PLAYER_TWO_WINS.contains(jugada21)){
            return jugada.get(1);
        }else{
            return jugada.get(0);
        }

    }

    public List jugarTorneo(List<List> torneo) throws WrongPlay, WrongNumberOfPlayers {
        if(torneo.get(0).get(0) instanceof String){
            return jugar(torneo);
        }else{
            List<List> ganador1 = jugarTorneo(torneo.get(0));
            List<List> ganador2 = jugarTorneo(torneo.get(1));
            return jugarTorneo(Arrays.asList(ganador1,ganador2));
        }
    }
}
